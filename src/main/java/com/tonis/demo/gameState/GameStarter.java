package com.tonis.demo.gameState;

import com.tonis.demo.services.GameService;
import com.tonis.demo.services.MapService;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;


@Data
@RequiredArgsConstructor
@Component
public class GameStarter {
    private final MapService mapService;
    private final GameService gameService;


    public long makeANewGame(Long mapId) {
        OneGame newGame = new OneGame(this.mapService.findMapById(mapId));
        return this.gameService.saveNewGame(newGame);
    }






}
