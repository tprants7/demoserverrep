package com.tonis.demo.gameState;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Data
@Component
public class DummyGameProvider {
    private List<OneGame> currentGames = new ArrayList<>();
    private long newId = 0;

    public long addNewGame(OneGame newGame) {
        newGame.setId(this.getNewId());
        this.currentGames.add(newGame);
        return newGame.getId();
    }

    private long getNewId() {
        return this.newId ++;
    }

    public OneGame findGameById(long id) {
        for(OneGame oneGame : currentGames) {
            if(oneGame.getId() == id) {
                return oneGame;
            }
        }
        return null;
    }

}
