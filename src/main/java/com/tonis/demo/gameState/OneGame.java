package com.tonis.demo.gameState;

import com.tonis.demo.map.Field;
import com.tonis.demo.map.Square;
import lombok.Data;

@Data
public class OneGame {
    private Long id;
    private Field map;
    private Square location;

    public OneGame(Field map) {
        this.map = map;
        this.location = map.getStartSquare();
    }

}
