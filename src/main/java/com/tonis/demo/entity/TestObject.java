package com.tonis.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TestObject {
    private String text;
    private int number;
}
