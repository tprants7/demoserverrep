package com.tonis.demo.map.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.tonis.demo.map.Field;

import java.io.IOException;

public class FieldIdSerializer extends StdSerializer<Field> {

    public FieldIdSerializer() { this(null);}

    public FieldIdSerializer(Class<Field> t) {super(t);}

    @Override
    public void serialize(Field value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeNumber(value.getId());
        gen.writeString(value.getName());
        /*gen.writeStartObject();
        gen.writeNumberField("id", value.getId());
        gen.writeStringField("terrainName", value.getTerrainName().name());
        gen.writeNumberField("xcoord", value.getXCoord());
        gen.writeNumberField("ycoord", value.getYCoord());
        gen.writeNumberField("settlementId", value.getSettlement().getId());
        gen.writeEndObject();*/

    }
}
