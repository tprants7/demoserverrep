package com.tonis.demo.map;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Field {
    private List<HorizontalLine> lines = new ArrayList<>();
    private String name;
    private long id;
    private Square startSquare;

    public Field(String name, long id) {
        this.name = name;
        this.id = id;
    }

    public HorizontalLine makeNewLine() {
        HorizontalLine newLine = new HorizontalLine((lines.size()));
        this.lines.add(newLine);
        return newLine;
    }

    public boolean setStartSquare(Square startSquare) {
        for(HorizontalLine oneLine : this.lines) {
            if(oneLine.getSquares().contains(startSquare)) {
                this.startSquare = startSquare;
                return true;
            }
        }
        return false;
    }


}
