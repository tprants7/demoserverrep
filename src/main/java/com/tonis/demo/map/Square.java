package com.tonis.demo.map;

import lombok.Data;

@Data
public class Square {
    private boolean wall;
    private int x;
    private int y;

    public Square(int x, int y, boolean isWall) {
        this.x = x;
        this.y = y;
        this.wall = isWall;
    }
}
