package com.tonis.demo.map;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class HorizontalLine {
    private List<Square> squares = new ArrayList<>();
    private int y;

    public HorizontalLine(int y) {
        this.y = y;
    }

    public Square makeNewSquareHere(boolean isWall) {
        Square newSquare = new Square(squares.size(), this.y, isWall);
        this.squares.add(newSquare);
        return newSquare;
    }
}
