package com.tonis.demo.map;

import lombok.Data;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Data
@Component
public class DummyMapProvider {
    private List<Field> savedFields = new ArrayList<>();


    @PostConstruct
    public void makeTestMaps() {
        savedFields.add(makeFirstMap());
    }

    public Field makeFirstMap() {
        Field newField = new Field("Test field", this.savedFields.size());
        HorizontalLine newLine = newField.makeNewLine();
        Square startSquare = newLine.makeNewSquareHere(false);
        newLine.makeNewSquareHere(false);
        newLine.makeNewSquareHere(false);
        newField.setStartSquare(startSquare);
        return newField;
    }
}
