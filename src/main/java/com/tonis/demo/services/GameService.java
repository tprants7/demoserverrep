package com.tonis.demo.services;

import com.tonis.demo.gameState.DummyGameProvider;
import com.tonis.demo.gameState.OneGame;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@RequiredArgsConstructor
@Data
@Component
public class GameService {
    private final DummyGameProvider gameProvider;

    public long saveNewGame(OneGame newGame) {
        return this.gameProvider.addNewGame(newGame);
    }

    public OneGame getGameById(long id) {
        return this.gameProvider.findGameById(id);
    }
}
