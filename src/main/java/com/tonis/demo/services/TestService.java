package com.tonis.demo.services;

import com.tonis.demo.entity.TestObject;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TestService {

    public String getTestMessage() {
        return "Hellow world";
    }

    public TestObject getTestObject() {
        return new TestObject("Hello world", 7);
    }
}
