package com.tonis.demo.services;

import com.tonis.demo.map.DummyMapProvider;
import com.tonis.demo.map.Field;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class MapService {
    private final DummyMapProvider dummyMapProvider;

    public Field findMapById(double id) {
        return this.dummyMapProvider.getSavedFields().get((int) id);
    }

    public List<Field> getAllFields() {
        return this.dummyMapProvider.getSavedFields();
    }
}
