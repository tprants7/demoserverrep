package com.tonis.demo.controllers;

import com.tonis.demo.gameState.GameStarter;
import com.tonis.demo.gameState.OneGame;
import com.tonis.demo.services.GameService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/game")
@RequiredArgsConstructor
public class GameController {
    private final GameService gameService;
    private final GameStarter gameStarter;

    @PostMapping("/newGame")
    public Long startANewGame(@RequestBody Long mapId) {
        return gameStarter.makeANewGame(mapId);
    }

    @GetMapping("/{id}")
    public OneGame getGame(@PathVariable long id) {
        return gameService.getGameById(id);
    }
}
