package com.tonis.demo.controllers;

import com.tonis.demo.map.Field;
import com.tonis.demo.services.MapService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/field")
@RequiredArgsConstructor
public class FieldController {
    private final MapService mapService;

    @GetMapping("/{id}")
    public Field getFieldById(@PathVariable Long id) {
        return this.mapService.findMapById(id);
    }

    @GetMapping
    public List<Field> getAllFields() {
        return this.mapService.getAllFields();
    }
}
