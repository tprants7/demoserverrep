package com.tonis.demo.controllers;

import com.tonis.demo.entity.TestObject;
import com.tonis.demo.services.TestService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
@RequiredArgsConstructor
public class TestController {
    private final TestService testService;

    @GetMapping
    public TestObject getTestObject() {
        return testService.getTestObject();
    }

}
